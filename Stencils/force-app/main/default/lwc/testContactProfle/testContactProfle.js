import { LightningElement, track } from 'lwc';
import userProfileStencil from './testContactProfileStencil.html';
import userProfile from './testContactProfle.html';

export default class UserProfile extends LightningElement {
  @track isLoading = true;

  render() {
    return this.isLoading ? userProfileStencil : userProfile;
  }

  renderedCallback() {
    // Simulating a long loading time until the component is ready to render.
    setTimeout(() => {this.isLoading = false;}, 5000);
  }
}